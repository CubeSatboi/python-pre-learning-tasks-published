def factors(number):
    # ==============
    # Your code here
    x=[]
    for i in range(2, number):
       if number % i == 0:
           x.append(i)
    if len(x)==0:
        x=str(number)+" is a prime number"
    return x

    # ==============

print(factors(15)) # Should print [3, 5] to the console
print(factors(12)) # Should print [2, 3, 4, 6] to the console
print(factors(13)) # Should print “13 is a prime number”
